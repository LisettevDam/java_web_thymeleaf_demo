package nl.bioinf.wis_on_thymeleaf.servlets;

import nl.bioinf.wis_on_thymeleaf.config.WebConfig;
import nl.bioinf.wis_on_thymeleaf.dao.DatabaseException;
import nl.bioinf.wis_on_thymeleaf.dao.MyAppDao;
import nl.bioinf.wis_on_thymeleaf.dao.MyAppDaoFactory;
import nl.bioinf.wis_on_thymeleaf.model.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DatabaseServlet", urlPatterns = "/list.user")
public class DatabaseServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final MyAppDao dataSource = MyAppDaoFactory.getDataSource();
        try {
            final User user = dataSource.getUser("Henk", "Henkie");
            final TemplateEngine templateEngine = WebConfig.getTemplateEngine();
            WebContext ctx = new WebContext(
                    request,
                    response,
                    request.getServletContext(),
                    request.getLocale());
            ctx.setVariable("user", user);
            templateEngine.process("user-details", ctx, response.getWriter());

        } catch (DatabaseException e) {
            e.printStackTrace();
        }

    }
}
