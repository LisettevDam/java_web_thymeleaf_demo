package nl.bioinf.wis_on_thymeleaf.servlets;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WelcomeServletTest {
    @Test
    @DisplayName("My first test")
    public void myFirstTest() {
        System.out.println("Test is running");
    }

}